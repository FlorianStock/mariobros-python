import pygame
from pygame.constants import K_1, KEYDOWN, QUIT
from Actors.anigomba import Anigomba
from components.level import Level
from Actors.mushroom import Mushroom
from Actors.player import Player
from enum import Enum
from mixer import sounds
import __main__
from os import path
class StateGame(Enum):    
    Playing = 1
    Die = 2
    Pause = 3
    Ending = 4
    End = 5
  
class Game:
    WIDTH = 800
    HEIGHT = 640
    SIZE = WIDTH,HEIGHT
    FPS = 60
    
    def __init__(self): 
          
        self.endingCoolDown = 430
        self.currentLevelNumber = 0
        self.levels = []
        self.actors = pygame.sprite.Group()
        self.levels.append(Level(fileName = path.join(__main__.CurrentPath,"resources/images/levels/level1.tmx")))
        self.levels.append(Level(fileName = path.join(__main__.CurrentPath,"resources/images/levels/level2.tmx")))
        self.currentLevel = self.levels[self.currentLevelNumber]
        self.StateGame = StateGame.Playing
        
        self.player = Player(x = self.currentLevel.startX, y = self.currentLevel.startY)
        self.player.currentLevel = self.currentLevel
        
        self.addMobs()
       
    def run(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.running = False               
        pygame.quit()
        
    def processEvents(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return True
            #Get keyboard input and move player accordingly
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_p:
                    if self.StateGame == StateGame.Pause:
                        self.StateGame = StateGame.Playing 
                    else :
                        self.StateGame = StateGame.Pause
                        self.pauseSound.play()             
                elif event.key == pygame.K_LEFT or event.key == pygame.K_q:
                    self.player.goLeft()
                elif event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                    self.player.goRight()
                elif event.key == pygame.K_UP or event.key == pygame.K_z:
                    self.player.jump()                       
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    return True;
                if (event.key == pygame.K_LEFT or event.key == pygame.K_q) and self.player.changeX < 0:
                    self.player.stop()
                elif (event.key == pygame.K_RIGHT or event.key == pygame.K_d) and self.player.changeX > 0:
                    self.player.stop()          
        return False
    
    def runLogic(self):
        
        #Decalage des entités quand le joueur avance...       
        if self.player.rect.right >= self.WIDTH - 400:
            difference = self.player.rect.right - (self.WIDTH  - 400)
            self.player.rect.right = self.WIDTH  - 400
            self.currentLevel.shiftLevel(-difference)
            for actor in self.actors:
                actor.rect.x -= difference
        if self.player.rect.left <= 200:
            difference = 200 - self.player.rect.left
            self.player.rect.left = 200
            self.currentLevel.shiftLevel(difference)
            for actor in self.actors:
                actor.rect.x += difference
          
        if self.StateGame == StateGame.Ending:
            self.endingCoolDown -= 1
            
                              
        if self.endingCoolDown <=0:
            self.nextGame()
            self.StateGame = StateGame.Playing 
            self.endingCoolDown = 430
            return
 
        if self.player.dieCoolDown <= 0:
            self.resetGame()
            self.StateGame = StateGame.Playing           
            return
        
        #Collisions with TILES LAYER 
        self.player.rect.y -= 2
        playerHitListTiles = pygame.sprite.spritecollide(self.player, self.currentLevel.layers[1].tiles, False)
        playerHitDecor = pygame.sprite.spritecollide(self.player, self.currentLevel.layers[0].tiles, False)    
        self.player.rect.y += 2 
        if len(playerHitListTiles) > 0:              
            for tile in playerHitListTiles:
                if tile.props != None:
                    if tile.props['type'] == "bloc_suprise_mushroom" and (len(tile.animations)>0):                       
                        tile.currentFrameAnimation = 0
                        tile.animations = []
                        sounds['bump'].play()
                        mushroom = Mushroom(tile.rect.x,tile.rect.y-32)
                        mushroom.currentLevel = self.currentLevel
                        self.actors.add(mushroom)
                    if tile.props['type'] == "destructible" :
                        if self.player.life>0:
                            self.currentLevel.layers[1].tiles.remove(tile)
                            self.player.changeY = 0
                            sounds['break'].play()                                                
                        else:
                            if pygame.mixer.Channel(1).get_busy() == True:
                                pygame.mixer.Channel(1).play( self.BumpSound, maxtime=2000)
        for tile in playerHitDecor:
                if tile.props != None:
                    if tile.props['type'] == "exit" and self.StateGame != StateGame.Ending:
                        self.player.rect.y = 900
                        sounds['stageClear'].play()
                        self.StateGame = StateGame.Ending    
                        
                           
        #Colisison with ACTORS !              
        playerHitActors = pygame.sprite.spritecollide(self.player, self.actors, True)                                     
        for actor in playerHitActors:
            if type(actor).__name__ == "Mushroom":
                self.player.up()
                actor.remove
            if type(actor).__name__ == "Anigomba":
                if self.player.rect.collidepoint(actor.rect.midtop) :               
                    actor.remove 
                    sounds["kick"].play()
                    self.player.changeY = -3   
                else:
                    self.player.changeY = -8
                    self.player.die = True
                    self.StateGame = StateGame.Die
                    sounds['die'].play()

                                                                                
        #if player over screen.. it's game over man !
        if self.player.rect.y > self.HEIGHT and self.StateGame != StateGame.Die and  self.StateGame != StateGame.Ending:
            self.player.changeY = -8
            self.player.die = True
            self.StateGame = StateGame.Die
            sounds['die'].play()    
            self.actors.clear
            
        # player can not move x if die..                                   
        if self.StateGame == StateGame.Die or self.StateGame == StateGame.Ending:
            self.player.changeX = 0
             
        if self.StateGame != StateGame.Pause:
            self.player.update()
            for actor in self.actors:
                actor.update()
            self.currentLevel.update()
            
    def nextGame(self):
        self.currentLevel = self.levels[self.currentLevelNumber+1]    
        self.player = Player(x =  self.currentLevel.startX, y = self.currentLevel.startY)
        self.player.currentLevel = self.currentLevel 
        self.StateGame = StateGame.Playing 
        self.addMobs()

    def resetGame(self):
        self.currentLevel.resetPositionLevel()                          
        self.player = Player(x = self.currentLevel.startX, y = self.currentLevel.startY)
        self.player.currentLevel = self.currentLevel  
        self.StateGame = StateGame.Playing
        self.addMobs() 
        
    def addMobs(self):
        self.actors.empty()
        for mob in self.currentLevel.objects:
            if mob.name == "Anigomba":   
                test = Anigomba(x = mob.x + self.currentLevel.levelShift, y = mob.y)           
                test.currentLevel = self.currentLevel               
                self.actors.add(test)  
                       
    def draw(self, screen):
        screen.fill((100,100,200))
        self.currentLevel.draw(screen)
        self.player.draw(screen)
        self.actors.draw(screen)
        pygame.display.flip()
        
    def load_image(self,file):
        self.file = file
        self.image = pygame.image.load(file)
        self.rect = self.image.get_rect()      
        self.screen = pygame.display.set_mode(self.rect.size)
        pygame.display.set_caption(f'size:{self.rect.size}')
        self.screen.blit(self.image, self.rect)
        pygame.display.update()