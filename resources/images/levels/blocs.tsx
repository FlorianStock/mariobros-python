<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="blocs" tilewidth="32" tileheight="32" tilecount="16" columns="4">
 <image source="../blocs.png" trans="ffffff" width="128" height="128"/>
 <tile id="0" type="bloc_suprise_mushroom">
  <animation>
   <frame tileid="0" duration="110"/>
   <frame tileid="1" duration="110"/>
   <frame tileid="2" duration="110"/>
   <frame tileid="3" duration="110"/>
  </animation>
 </tile>
 <tile id="4" type="bloc_suprise_coins">
  <animation>
   <frame tileid="4" duration="110"/>
   <frame tileid="5" duration="110"/>
   <frame tileid="6" duration="110"/>
   <frame tileid="7" duration="110"/>
  </animation>
 </tile>
</tileset>
