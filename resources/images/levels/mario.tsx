<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="mario" tilewidth="32" tileheight="32" tilecount="160" columns="8">
 <image source="level_mario.png" trans="ffffff" width="256" height="640"/>
 <tile id="1" type="destructible"/>
 <tile id="25" type="water">
  <animation>
   <frame tileid="25" duration="210"/>
   <frame tileid="26" duration="210"/>
  </animation>
 </tile>
 <tile id="57" type="destructible"/>
 <tile id="65" type="destructible"/>
 <tile id="126" type="player"/>
 <tile id="154" type="exit"/>
</tileset>
