import pygame

class SpriteSheet(object):
    def __init__(self, fileName):
        self.sheet = pygame.image.load(fileName)

    def image_at(self, rectangle):
        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size, pygame.SRCALPHA, 16).convert_alpha()
        image.blit(self.sheet, (0, 0), rect)
        
        self.size = image.get_size()
        
        return image
        #return  pygame.transform.scale(image, (int(self.size[0]*2), int(self.size[1]*2)))