import pygame

class Layer(object):
    def __init__(self, index, mapObject):
        #Layer index from tiled map
        self.index = index
        
        #Create gruop of tiles for this layer
        self.tiles = pygame.sprite.Group()
        
        #Reference map object
        self.mapObject = mapObject
                      
        #Create tiles in the right position for each layer
        for x in range(self.mapObject.width):
            for y in range(self.mapObject.height):
                img = self.mapObject.get_tile_image(x, y, self.index) 
                props = self.mapObject.get_tile_properties(x, y, self.index)
                img = self.mapObject.get_tile_image(x, y, self.index)             
                if img:
                    tile = Tile(image = img, x = (x * self.mapObject.tilewidth), y = (y * self.mapObject.tileheight),props = props)
                    
                    if props != None:                       
                        if len(props['frames']) > 0: 
                            tile.animations = props['frames']
                            
                    self.tiles.add(tile)

    #Draw layer
    def draw(self, screen):
        self.tiles.draw(screen)
        
        
class Tile(pygame.sprite.Sprite):
    def __init__(self, image, x, y, props):
        pygame.sprite.Sprite.__init__(self)
        
        image.set_colorkey((255,0,255)) 
        self.props = props
        
        #By default, no animation for tiles..
        self.animations = []
        self.currentFrameAnimation = 0
        self.currentDurationAnimation = 0
           
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
    
 