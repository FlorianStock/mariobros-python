
import pytmx

from components.layer import Layer

class Level(object):
    def __init__(self, fileName):
        #Create map object from PyTMX
        
        self.mapObject = pytmx.load_pygame(fileName)
        
        #Create list of layers for map
        self.layers = []
          
        #Amount of level shift left/right
        self.levelShift = 0
        
        self.layers.append(Layer(index = 0, mapObject = self.mapObject))
        self.layers.append(Layer(index = 1, mapObject = self.mapObject))
              
        start = self.mapObject.get_object_by_name("player").as_points

        self.startX= start[0].x
        self.startY= start[0].y
        
        self.objects = self.mapObject.objects
                    
    def resetPositionLevel(self):
        self.levelShift = 0
        self.layers = []
        self.objects = self.mapObject.objects
        self.layers.append(Layer(index = 0, mapObject = self.mapObject))#obstacles
        self.layers.append(Layer(index = 1, mapObject = self.mapObject))#decors

    #Move layer left/right
    def shiftLevel(self, shiftX):
        self.levelShift += shiftX
        
        for layer in self.layers:
            for tile in layer.tiles:
                tile.rect.x += shiftX
    
    #Update layer
    def draw(self, screen):
        for layer in self.layers:
            layer.draw(screen)
    
    def update(self):              
        for layer in self.layers:
            for tile in layer.tiles:
                if tile.props != None:
                    
                    #Animation for tiles..
                    if (len(tile.animations) > 0 and tile.props['type'] != ""):
                        tile.currentDurationAnimation += 10                        
                        if tile.animations[tile.currentFrameAnimation].duration <= tile.currentDurationAnimation:
                            tile.currentDurationAnimation = 0
                            tile.currentFrameAnimation += 1
                            if tile.currentFrameAnimation > len(tile.animations)-1:
                                tile.currentFrameAnimation = 0
                            tile.image = layer.mapObject.images[tile.animations[tile.currentFrameAnimation].gid]
                        