import  __main__
from os import path
import pygame
import random
print(random.randint(0,9))
from components.SpriteSheet import SpriteSheet

class Mushroom(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        
        image =  pygame.image.load(path.join(__main__.CurrentPath,"resources/images/mushroom.png")).convert()

        self.image = pygame.transform.scale(image, (32, 32))
        self.rect = self.image.get_rect()
       
        self.speed = 1
        self.rect.x = x
        self.rect.y = y

        self.changeY = 0
        self.currentLevel = None
        
        self.direction = "right" if random.randint(0,1) == 0 else "left"
        
    def update(self):
        
        if self.direction == "right":
            self.rect.x += self.speed
        else:
            self.rect.x -= self.speed
        
        self.rect.y += self.changeY
        
        tileHitList = pygame.sprite.spritecollide(self, self.currentLevel.layers[1].tiles, False)
       
        #If there are tiles in that list
        if len(tileHitList) > 0:
            for tile in tileHitList:                 
                self.changeY = 0           
                if self.rect.collidepoint(tile.rect.midleft): 
                    self.direction = "left"
                    self.rect.x -=1
                elif self.rect.collidepoint(tile.rect.midright): 
                    self.direction = "right"
                    self.rect.x +=1
                    print(self.direction)
                else:
                    if self.rect.collidepoint(tile.rect.midbottom):
                        print(self.direction)
                        self.rect.bottom = tile.rect.top 
        else:
            self.changeY += 0.2
            
    def draw(self, screen):
        screen.blit(self.image, self.rect)