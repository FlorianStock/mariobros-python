import pygame
from mixer import sounds
from components.SpriteSheet import SpriteSheet
from os import path
import __main__ 

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 640

class Player(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
           
        self.life = 0
        self.startX = x
        self.startY = y
        #Load the spritesheet of frames for this player
        self.sprites = SpriteSheet(path.join(__main__.CurrentPath,"resources/images/player.png"))
    
        self.stillRight = self.sprites.image_at((0, 0, 30, 42))
        self.stillLeft = self.sprites.image_at((0, 42, 30, 42))
        
        #List of frames for each animation
        self.runningRight = (self.sprites.image_at((0, 84, 30, 42)),
                    self.sprites.image_at((30, 84, 30, 42)),
                    self.sprites.image_at((60, 84, 30, 42)),
                    self.sprites.image_at((90, 84, 30, 42)),
                    self.sprites.image_at((120, 84, 30, 42)))

        self.runningLeft = (self.sprites.image_at((0, 126, 35, 42)),
                    self.sprites.image_at((30, 126, 30, 42)),
                    self.sprites.image_at((60, 126, 30, 42)),
                    self.sprites.image_at((90, 126, 30, 42)),
                    self.sprites.image_at((120, 126, 30, 42)))
                    
        self.jumpingRight = (self.sprites.image_at((30, 0, 30, 42)),
                    self.sprites.image_at((60, 0, 30, 42)),
                    self.sprites.image_at((90, 0, 30, 42)))

        self.jumpingLeft = (self.sprites.image_at((30, 42, 30, 42)),
                    self.sprites.image_at((60, 42, 30, 42)),
                    self.sprites.image_at((90, 42, 30, 42)))
        
        
        self.image = self.stillRight
        
        #Set player position
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        
        #get if player can move mario...
         
        #Set speed and direction
        self.changeX = 0
        self.changeY = 0
        self.direction = "right"
        
        #Boolean to check if player is running, current running frame, and time since last frame change       
        self.running = False
        self.runningFrame = 0
        self.runningTime = pygame.time.get_ticks()
        
        self.die = False
        self.dieCoolDown = 150
        
        #Players current level, set after object initialized in game constructor
        self.currentLevel = None
     
        self.size = self.image.get_size()
        self.bigger_img = pygame.transform.scale(self.image, (int(self.size[0]*1.5), int(self.size[1]*1.5)))
        self.bigrect = self.bigger_img.get_rect()
        
    def update(self):
        
        #Update player position of X if player is alive 
        if self.die == False:          
            self.rect.x += self.changeX
        else:
            self.dieCoolDown -= 1
                              
        #Get tiles in collision layer that player is now touching
        tileHitList = pygame.sprite.spritecollide(self, self.currentLevel.layers[1].tiles, False)
        
        #Move player to correct side of that block
        for tile in tileHitList:
            if self.changeX > 0:
                self.rect.right = tile.rect.left
            else:
                self.rect.left = tile.rect.right
        
        #Move screen if player reaches screen bounds           
        #Update player position by change
        self.rect.y += self.changeY
        
        #Get tiles in collision layer that player is now touching
        tileHitList = pygame.sprite.spritecollide(self, self.currentLevel.layers[1].tiles, False)
       
        #If there are tiles in that list
        if len(tileHitList) > 0:
            #Move player to correct side of that tile, update player frame
            for tile in tileHitList:
               
                if self.changeY > 0:
                    self.rect.bottom = tile.rect.top
                    self.changeY = 1
                                          
                    if self.direction == "right":
                        self.image = self.stillRight
                    else:
                        self.image = self.stillLeft
                else:
                    self.changeY = 0                  
                    self.rect.top = tile.rect.bottom
                    
        #If there are not tiles in that list
        else:
            #Update player change for jumping/falling and player frame
            self.changeY += 0.2
            if self.changeY > 0:
                if self.direction == "right":
                    self.image = self.jumpingRight[1]
                else:
                    self.image = self.jumpingLeft[1]
        
        #If player is on ground and running, update running animation
        if self.running and self.changeY == 1:
            if self.direction == "right":
                self.image = self.runningRight[self.runningFrame]
            else:
                self.image = self.runningLeft[self.runningFrame]
        
        #When correct amount of time has passed, go to next frame
        if pygame.time.get_ticks() - self.runningTime > 50:
            self.runningTime = pygame.time.get_ticks()
            if self.runningFrame == 3:
                self.runningFrame = 0
            else:
                self.runningFrame += 1
    #When player catch mushroom              
    def up(self):
        self.life +=1
        y = self.rect.y
        x = self.rect.x
        self.rect = self.bigger_img.get_rect()
        self.rect.y = y
        self.rect.x = x
        sounds['eat'].play()
        #self.rect.inflate(0,50)
        #self.rect.y = self.rect.y + 50
     
    #Make player jump
    def jump(self):
        #Check if player is on ground
        self.rect.y += 2
        tileHitList = pygame.sprite.spritecollide(self, self.currentLevel.layers[1].tiles, False)
        self.rect.y -= 2
        
        if len(tileHitList) > 0:
            if self.direction == "right":
                self.image = self.jumpingRight[0]
            else:
                self.image = self.jumpingLeft[0]
                
            self.changeY = -8
            pygame.mixer.Channel(3).play( sounds['jump'], maxtime=2000)  
 
    #Move right
    def goRight(self):
        self.direction = "right"
        self.running = True
        self.changeX = 3
    
    #Move left
    def goLeft(self):
        self.direction = "left"
        self.running = True
        self.changeX = -3
    
    #Stop moving
    def stop(self):
        self.running = False
        self.changeX = 0
    
    #Draw player
    def draw(self, screen):
        if self.life == 0:
            screen.blit(self.image, self.rect)
        else:
            self.bigger_img = pygame.transform.scale(self.image, (int(self.size[0]*1.5), int(self.size[1]*1.5)))
            screen.blit(self.bigger_img, self.rect)