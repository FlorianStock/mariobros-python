import pygame
from pygame.constants import K_1, KEYDOWN, QUIT
from components.level import Level
import sys
import os

 
if getattr(sys, 'frozen', False): # PyInstaller adds this attribute
    # Running in a bundle
    CurrentPath = sys._MEIPASS
else:
    # Running in normal Python environment
    CurrentPath = os.path.dirname(__file__)
    
from game import Game, StateGame

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 640

pygame.init()
    
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), pygame.RESIZABLE)
pygame.display.set_caption("Mario bros like !!!")
clock = pygame.time.Clock()
done = False

game = Game()
game.StateGame = StateGame.Playing
    
while not done:
    done = game.processEvents()
    game.runLogic()
    game.draw(screen)
        
    for event in pygame.event.get():
        if event.type == pygame.VIDEORESIZE:
            old_surface_saved = screen
            screen = pygame.display.set_mode((event.w, event.h), pygame.RESIZABLE)
            screen.blit(old_surface_saved, (0,0))
            del old_surface_saved
                
                
    clock.tick(60)
        
pygame.quit()
    




 

 
